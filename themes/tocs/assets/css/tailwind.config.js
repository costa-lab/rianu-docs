module.exports = {
    darkMode: 'class',
    theme: {
      extend: {
        textOpacity: ['dark']
      }
    },
    variants: {},
    plugins: []
  }